# Member Stations Node.js and Angular Application

Requirements
* Node.js 16.4.x
* NVM (node version manager for mac or windows)

Node.js Version = 16.x. Use Node Version manager and always run `nvm use` to load correct nodejs version stored in .nvmrc. Or just run `nvm use 16.4.0`

Getting started

```
nvm use
npm i
npm start
# then load <http://localhost:3000/> or <http://localhost:3000/api/nf-api-v2>
```

This will compile the typescript server and run via nodemon and will compile the typescript angular application and run it using the angular dev proxy.

## Run like Prod

```
ng build --prod
npm run compile-server
nodemon ./dist/server/index.js
```
