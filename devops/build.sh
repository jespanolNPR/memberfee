#!/bin/sh

# get build number (bamboo) from first argument
build=$1
branch=$2

echo "build $build and branch $branch"

# tells script to exit early on a command Error (non zero return)
#set -e

. ~/.nvm/nvm.sh
nvm install 16.4.0
nvm use 16.4.0

echo 'node -v'
node  -v

echo 'npm -v'
npm -v

echo 'npm install for nf-client project'
npm install --no-progress
echo 'npm install finished'

# remove current dist folder files
echo 'Removing current ./dist folder'
rm -rf ./dist
mkdir ./dist

rm -rf artifacts
mkdir -p artifacts

npm run compile-server

# Creates dist/app files
echo 'Running ng build --prod'
$(npm bin)/ng build --prod
echo 'ng build finished'

echo 'npm prune --production'
npm prune --production

# copy over to dist
cp -rf ./node_modules ./dist/node_modules

zipFileName=memberstations-node-$version.zip
zip -r -q "./artifacts/$zipFileName" dist devops
echo "Created ZIP file for dist at ${zipFileName}"
