import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class SalesforceService {




  apiUrl = "http://localhost:7203/api/salesforce-api-v1/";
  constructor(private http: HttpClient) { }

  getUserValidation(id){

    return this.http.get(this.apiUrl + 'UserValidation/' +id);
  }
  getAuthSigner(email) {
    return this.http.get(this.apiUrl + 'AuthSigner/'+email);
  }
  getOperatorFYServices(id) {
    return this.http.get(this.apiUrl + 'OperatorFYServices/'+ id);
  }
  getQuotes(opId) {
    return this.http.get(this.apiUrl + 'getQuote/' + opId);
  }
  getQuoteLines(id){
    return this.http.get(this.apiUrl + 'getQuoteLines/' + id);
  }
  getOpportunity(id) {
    return this.http.get(this.apiUrl + 'getOpportunity/' + id);
  }
  getOpportunityDetails(){
    return this.http.get(this.apiUrl + 'getOpportunityDetails');
  }
  getAccount(id){
    return this.http.get(this.apiUrl + 'getAccount/'+ id);
  }
  getUser(id){
    return this.http.get(this.apiUrl + 'getUser/' + id);
  }
  updateOpportunity(id,stageName,FY_OPId,submittedBy){
    return this.http.get(this.apiUrl + 'submitOpportunity/' + id + '/'+ stageName + '/' + FY_OPId + '/' + submittedBy )
  }


}