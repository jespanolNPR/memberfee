export  class authorizedSigner  {

    Email: string;
    FirstName: string;
    LastName: string;
    Address__c: string;
    Billing_Email__c: string;
    City__c: string;
    Core_Fee_CY_Capped__c: number;
    Fiscal_Yr__c: string;
    Operator_Broadcast_Stations__c: string;
    Org_Responsible_for_Paying__c: string;
    Responsible_Party_Email__c: string;
    Responsible_Party__c: string;
    State__c: string;
    Zip__c: string;
    Account_Owner: string;
    Account_Owner_Email: string;
    Account_Owner_Phone: string;
    Operator_Name: string;
    AllowFeeSummary: boolean;
    OpportunityId: string;
    QuoteId: string;
    FY_Financial_Id: string;
   
    
}