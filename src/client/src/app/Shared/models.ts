export interface MemberFee {
}

export  interface AuthorizedSigner extends MemberFee  {

    Email: string;
    FirstName: string;
    LastName: string;
    Address__c: string;
    Billing_Email__c: string;
    City__c: string;
    Core_Fee_CY_Capped__c: number;
    Fiscal_Yr__c: string;
    Operator_Broadcast_Stations__c: string;
    Org_Responsible_for_Paying__c: string;
    Responsible_Party_Email__c: string;
    Responsible_Party__c: string;
    State__c: string;
    Zip__c: string;
    Account_Owner: string;
    Account_Owner_Email: string;
    Account_Owner_Phone: string;
    Operator_Name: string;
    AllowFeeSummary: boolean;
    OpportunityId: string;
    QuoteId: string;
    FY_Financial_Id: string;
   
    
}

export interface User extends MemberFee{
    FirstName: string;
    LastName: string;
}

export  interface FYServices extends MemberFee  {

   ServiceName: string;
   ServiceType: string;
   ServiceStations: string;

    
}

export  interface Opportunity extends MemberFee  {
    Id: string;
    Name: string;
    NPRStations_Validated__c: boolean;
    StageName: string;
    AccountId: string;
    Fiscal_Year__c: string;
 
     
 }


export  interface OpportunityProducts extends MemberFee  {

    ProductName: string;
    ProductPrice: string;
    ProductIncluded: boolean;
 
     
 }


export interface MemberFee{}