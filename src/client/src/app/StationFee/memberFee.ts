import { Component, OnInit } from '@angular/core';
import { SalesforceService } from '../Shared/Salesforce.service';
import { AuthorizedSigner } from '../Shared/models';
import {authorizedSigner } from "../Shared/authorizedSigner";
import {fyServices } from "../Shared/fyServices";
import {opportunity} from "../Shared/opportunity";
import {opportnityProducts } from "../Shared/opportunityProducts";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-root',
templateUrl: './memberFee.html',
//templateUrl: './programSummary.html',
  styleUrls: ['./MemberFee.css']
})
export class MemberFee  implements OnInit   {

  constructor(private api: SalesforceService,
    ) {   
       this.getUserValidation(this.id);    
        
         
    }

  //added by Don for current date display
  thisdate = new Date();

  title = 'Member Fee';
  authSigner:any =[];
  operatorFYServices:any=[];
  model: AuthorizedSigner;
  fyServices: fyServices;
  opportnityProducts: opportnityProducts;
  opportunity: opportunity;
  arrServices: string [];
  arrProducts: string[];
  //variable for displaying Newscast broadcast service
  hasNewsMag = false;
  //variables fo rural/minority discount language

  ruralDiscount = false;
  minorityDiscount = false;
  rurMin = '';
    showList : [];
  totalNonNews=0;
  totalCoreFee=0;
  totalFee=0;
  totalFeeWith1Perc = 0;
  msg_RuralMinority = "";
   noAccess = false;
  loading = false;
  loadPage=false;
subsmitted:false;
hideOnePerc: true;


 
  //starting point - replace with query string
//  id= "003C000001dFpeMIAS"; //ellen's conytact
id="0031A00001yeWwJQAU";//wglt rc mcbride

//KRWG
//  id="0038000000sFk4wAAC";//AREP adrian velarde;
//id="0030h00002eAJdkAAG"//marcus loyola

  ngOnInit() {
    this.model = new authorizedSigner();
    this.fyServices= new fyServices();
    this.opportnityProducts = new opportnityProducts();
    this.opportunity = new opportunity();
    
   

  }
  getUserValidation(id) {
    this.loadPage=false;
    var sample;
    this.api.getUserValidation(id)
      .subscribe(data => {
        sample=JSON.stringify(data);
        this.authSigner=sample;
        this.model.Responsible_Party_Email__c = data[0].Email;
        this.model.FirstName = data[0].FirstName;
        this.model.LastName = data[0].LastName;
        this.getAuthSigner( this.model.Responsible_Party_Email__c);
       
      });
  }

  getAuthSigner(email) {
    var sample;
    this.api.getAuthSigner(email)
      .subscribe(data => {
        sample=JSON.stringify(data);
       this.authSigner=sample;
        if (data[0].NoRecord === undefined){
        this.model.Billing_Email__c = data[0].Billing_Email__c;
        this.model.Address__c = data[0].Address__c;
        this.model.City__c = data[0].City__c;
        this.model.State__c = data[0].State__c;
        this.model.Zip__c = data[0].Zip__c;
       this.getAccount(data[0].Operator__c);
        this.getOperatorFYServices(data[0].Operator__c);
        this.getOpportunity(data[0].Operator__c);
       
       
        
      
      }else{
        this.noAccess = true;
        this.loadPage=true;
      }

      
        
      });
  }



  getUser(id){
      this.api.getUser(id)
      .subscribe(data => {
        this.model.Account_Owner=data[0].Name;
        this.model.Account_Owner_Email=data[0].Email;
        this.model.Account_Owner_Phone=data[0].Phone;
      });

  }

  getAccount(id){
    this.api.getAccount(id)
    .subscribe(data => {
      this.model.Operator_Name=data[0].Name;
      this.getUser(data[0].OwnerId);
    });

}

getOpportunity(id){
  this.api.getOpportunity(id)
  .subscribe(data => {
    this.model.OpportunityId=data[0].Id;
    this.opportunity.Id=data[0].Id;;
    this.opportunity.NPRStations_Validated__c=data[0].NPRStations_Validated__c;
    this.opportunity.StageName = data[0].StageName;
    if ( data[0].StageName==="Proposal" ) {
        this.opportunity.submitted=false; 
     
    }
    else if( data[0].StageName==="Approved" || data[0].StageName==="Submitted" ) {
        this.opportunity.submitted=true;
        this.hideOnePerc=true;
    }
    this.getQuote(data[0].Id);
  });

  

}

onConsentChecked(eve: any) {
    this.opportunity.submitted= !this.opportunity.submitted;
   
  }

onPaidInFullClciked(eve: any) {
    this.opportunity.paidInFull= !this.opportunity.paidInFull;

  }

  submitFee(){
    this.loading=true;
    this.opportunity.StageName="Submitted"
    var paid_InFull;
    var submittedBy  = this.model.FirstName + " " + this.model.LastName;
    if (this.opportunity.paidInFull === true){
        paid_InFull=this.model.FY_Financial_Id;
    }else{
        paid_InFull="No";
    }
    this.api.updateOpportunity(this.opportunity.Id, "Submitted",paid_InFull,submittedBy)
    .subscribe(data => {
       
       this.opportunity.submitted = true;
       this.opportunity.StageName="Submitted"
       this.loading=false;
      });
    
  }


getQuote(opId){
  this.api.getQuotes(opId)
  .subscribe(data => {
   
    this.model.QuoteId=data[0].Id;
    this.getQuotesDetails(data[0].Id);
  });

}

  getOperatorFYServices(id) {
   
    this.api.getOperatorFYServices(id)
      .subscribe(data => {
      
        this.arrServices = data as string [];
       var temp_arrServices = { serviceName: "",serviceType:"",serviceStations:"" ,airNPRNewsmag:"", Org_Responsible_for_Paying__c:"", Minority_Discount__c:"", Rural_Discount__c:""}
        temp_arrServices = data as any;
        for (var i in data) {
          this.model.FY_Financial_Id = data[i].FY_Operator_Financials__c;
          temp_arrServices[i].AirNPRNewsmag__c = data[i].AirNPRNewsmag__c;
          temp_arrServices[i].FY_Operator_Financials__r.Org_Responsible_for_Paying__c = data[i].FY_Operator_Financials__r.Org_Responsible_for_Paying__c;

          temp_arrServices[i].serviceName = data[i].Service__r.Name;

          temp_arrServices[i].Minority_Discount__c = data[i].FY_Operator_Financials__r.Minority_Discount__c;
          temp_arrServices[i].Rural_Discount__c = data[i].FY_Operator_Financials__r.Rural_Discount__c;

          if(temp_arrServices[i].Broadcast_Service_Type__c==="Broadcast News"){
            temp_arrServices[i].Broadcast_Service_Type__c = "News";
           this.model.Responsible_Party__c = data[0].FY_Operator_Financials__r.Responsible_Party__r.FirstName + " " + data[0].FY_Operator_Financials__r.Responsible_Party__r.LastName;
            this.model.Org_Responsible_for_Paying__c = data[i].FY_Operator_Financials__r.Org_Responsible_for_Paying__c;
          }else{
            temp_arrServices[i].Broadcast_Service_Type__c = "Music"
          }

          if(temp_arrServices[i].AirNPRNewsmag__c){
            temp_arrServices[i].Broadcast_Service_Type__c = 'News';
          }

          //ADDED  BY DON FOR WGLT EXCEPTION, USE id="0031A00001yeWwJQAU"; rc mcbride
          if(temp_arrServices[i].Service__r.Name =='WGLT'){
            temp_arrServices[i].Service_Broadcast_Stations__c = 'WCBU-FM,WGLT-FM';
          }

        }
        this.arrServices = temp_arrServices as any;
        //console.log(this.arrServices[0]);

        //get newsmag, rural, and minority discount vals
        this.hasNewsMag = data[0].AirNPRNewsmag__c;
        this.ruralDiscount = data[0].Rural_Discount__c;
        this.minorityDiscount =  data[0].Minority_Discount__c;


        if(this.ruralDiscount && this.minorityDiscount){
          this.rurMin = 'rural/minority';
        }
        if(this.ruralDiscount && !this.minorityDiscount){
          this.rurMin = 'rural';
        }
        if(!this.ruralDiscount && this.minorityDiscount){
          this.rurMin = 'minority';
        }
        if(this.ruralDiscount || this.minorityDiscount){
          this.msg_RuralMinority =  'Fee includes a discount as a '+this.rurMin+'-area service station.';
        }

    //sort by service name
    var sortService = this.arrServices.sort(sortByService); 
    this.arrServices= sortService;


      });

      function sortByService( a, b ) {
        if ( a.serviceName < b.serviceName ){
          return -1;
        }
        if ( a.serviceName > b.serviceName){
          return 1;
        }
        return 0;
      }
  }

  

  getQuotesDetails(id) {





    this.api.getQuoteLines(id)
      .subscribe(data => {
        var temp_arrProducts={productName: "",tier:"",subtotal:"" ,price:"", broadcastService:"", onboard: "false",link:"",startDate:"",dropDate:"",endDate:""};
        temp_arrProducts= data as any;
        this.loadPage=true;
       
        var c = {productName: "",tier:"",subtotal:"" ,price:"", broadcastService:"", onboard: "false", link:"",startDate:"",dropDate:"",endDate:""},
      
        arrayList = [], obj_c_processed = [];
  
         c = temp_arrProducts ;

        //  console.log(c);
       
        for (var j in c){
          if( c[j].productName!== "Core Fee" ){
          this.totalNonNews= Math.round(this.totalNonNews+c[j].price);
          this.totalFee = this.totalFee + c[j].price;
          }
          if( c[j].productName=== "Core Fee" ){
            this.totalCoreFee= Math.round(this.totalCoreFee+c[j].price);
            this.totalFee = Math.round(this.totalFee + c[j].price);
            }
        }
      this.totalFeeWith1Perc = Math.round(this.totalFee - this.totalFee/100);
    //DON'S ARRAY

    //ARRAY FOR SHOW ORDER
    let showArray: Array<{id: string, show: string, link: string}> = [
      {id:"01tC0000003vckyIAA",show:"All Songs Considered",link:""},
      {id:"01tC0000003vckzIAA",show:"alt.Latino",link:""},
      {id:"01t1A000004ePagQAE",show:"1A",link:"/programinfo/1a/index.cfm"},
      {id:"01tC0000003uNQKIA2",show:"Bullseye",link:"/programinfo/bullseye/bullseye_index.cfm"},
      {id:"01t0h000006LH1oAAG",show:"Code Switch and Life Kit",link:"http://dc-mpsdev01/programinfo/CS_LK/index.cfm"},
      {id:"01tC0000004L44IIAS",show:"Conversations from the World Cafe",link:"/programinfo/world_cafe/index.cfm"},
      {id:"01tC0000003uNQeIAM",show:"Fresh Air",link:"/programinfo/freshair/index.cfm"},
      {id:"01tC0000003uNQjIAM",show:"Fresh Air Weekend",link:"/programinfo/freshair/index.cfm"},
      {id:"01tC0000003uNQoIAM",show:"From the Top",link:"/programinfo/fromthetop/index.cfm"},
      {id:"01tC0000003uNQtIAM",show:"HD - NPR Programs",link:""},
      {id:"01tC0000003uNQyIAM",show:"Here & Now",link:"/programinfo/hereandnow/index.cfm"},
      {id:"01t1A0000056PskQAE",show:"Hidden Brain",link:"/programinfo/hidden_brain/index.cfm"},
      {id:"01tC0000003uNR3IAM",show:"Holiday Specials",link:""},
      {id:"01t1A0000056PspQAE",show:"It's Been a Minute with Sam Sanders",link:"/programinfo/sam_sanders/index.cfm"},
      {id:"01tC0000003vcl9IAA",show:"Jazz Night in America",link:"/programinfo/jazznight/index.cfm"},
      {id:"01tC0000003uNRcIAM",show:"Mountain Stage",link:"/programinfo/mountain_stage/index.cfm"},
      {id:"01tC0000003uNRhIAM",show:"NPR Newscasts",link:"/programinfo/newscast/index.cfm"},
      {id:"01t1A0000056PsfQAE",show:"Planet Money and How I Built This",link:"/programinfo/PM_HIBT/index.cfm"},
      {id:"01tC0000003uNTuIAM",show:"TED Radio Hour",link:"/programinfo/ted_radio/index.cfm"},
      {id:"01tC0000003uNUOIA2",show:"The Thistle & Shamrock",link:"/programinfo/thistleshamrock/index.cfm"},
      {id:"01t0h000006LH1pAAG",show:"Throughline",link:"/programinfo/throughline/index.cfm"},
      {id:"01tC0000003uNUTIA2",show:"Wait Wait... Don't Tell Me!",link:"/programinfo/waitwait/index.cfm"},
      {id:"01tC0000003uNW1IAM",show:"World Cafe (2 hours)",link:"/programinfo/world_cafe/index.cfm"},
      {id:"01tC0000003uNWbIAM",show:"World Cafe (5 hours)",link:"/programinfo/world_cafe/index.cfm"}];
// for(let show of showArray){
//   console.log(show);
// }

// for(let s=0;s<showArray.length;s++){
//   console.log(showArray[s]);
// }


      //LOOP THROUGH SHOWARRAY AND ADD VALUES FROM API
      var hasFreshAir = false;
      var hasWrdCaf5 = false;
      for(let s=0;s<showArray.length;s++){
        var sho = showArray[s];
        var shoNme = sho.show;
        var shoTier = '';
        var shoSubTot = '';
        var shoPrc = '';
        var shoSrv = '';
        var shoLnk= sho.link;
        var dateStart = '';
        var dateDrop = '';
        var dateEnd = '';
        //FIND API VALUES
        for (var j in c){
              if(c[j].productName == shoNme){
                shoTier = c[j].tier;
                shoSubTot = c[j].subtotal;
                shoPrc = c[j].price;
                shoSrv = c[j].broadcastService;
                shoLnk = shoLnk;
                dateStart = c[j].startDate;
                dateDrop = c[j].dropDate;
                dateEnd = c[j].endDate;
              }
        }

        //CHECK OFF FRESH AIR
        if(shoNme == 'Fresh Air Weekend'){
          for (var j in c){
            if (c[j].productName == "Fresh Air"){
              shoSrv = 'Included with Fresh Air';
            }
          }
        }

        if(shoNme == 'NPR Newscasts' && this.hasNewsMag){
          shoSrv = 'Included for news services';
       }

        //CHECK OFF Conversations world cafe
        if(shoNme == 'Conversations from the World Cafe'){
          for (var j in c){
            if (c[j].productName == "World Cafe (5 hours)"){
              shoSrv = 'Included with World Cafe';
            }
          }
        }


       arrayList.push({productName: shoNme, tier: shoTier, subtotal: shoSubTot, price: shoPrc ,broadcastService:shoSrv, link:shoLnk,startDate:dateStart,dropDate:dateDrop,endDate:dateEnd});

     }


      



      

      
    //var sorted = arrayList.sort(sortByProgram); 
    //this.arrProducts= sorted;
    this.arrProducts = arrayList;
       
      });

      function sortByProgram( a, b ) {
        if ( a.productName < b.productName ){
          return -1;
        }
        if ( a.productName > b.productName ){
          return 1;
        }
        return 0;
      }


  }




}







