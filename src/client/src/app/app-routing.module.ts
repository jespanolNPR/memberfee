import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberFee } from './StationFee/memberFee';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
  path: 'home',
  component:AppComponent,
  children: [
      {
          path: '', 
              children:[
                  { path: 'memberfee/:cid', component: MemberFee},
              ]
      }
  ]
}

//working path
// {
//   path: 'home',
//   component:AppComponent,
//   children: [
//       {
//           path: 'memberfee/:cid', component: MemberFee
              
//       }
//   ]
// }
// { path: 'home', component: AppComponent },
// { path: 'memberfee/:cid', component: MemberFee }
 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 


}
