import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { MemberFee } from './StationFee/memberFee';

import { SalesforceService } from './Shared/Salesforce.service';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';





@NgModule({
  declarations: [
    
    MemberFee,
    AppComponent
  ],
  imports: [
   
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [SalesforceService],
  bootstrap: [MemberFee]
})
export class AppModule { }
