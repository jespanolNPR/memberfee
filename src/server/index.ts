/*
	Entry Point into sever code
*/
var CONFIG = require('config');

const logger = console;

/*import * as logger from 'winston';

// You can override the config logging level via the yaml files
let logLevel = 'debug';
if (CONFIG && CONFIG.log && CONFIG.log.level) {
  logLevel = CONFIG.log.level;
}
*/

// logger.remove(logger.transports.Console);
// logger.add(logger.transports.Console, {
//   level: logLevel,
//   colorize: true,
//   timestamp: true,
//   humanReadableUnhandledException: true,
//   json: false
// });

// MUST happen before any imports
CONFIG.environment = CONFIG.util.getEnv('NODE_ENV') || 'dev';

logger.info('NODE_ENV: ' + CONFIG.environment);
logger.info('NODE_CONFIG_DIR: ' + CONFIG.util.getEnv('NODE_CONFIG_DIR'));
logger.info(CONFIG);

// You can also pass PORT Like this:
//  NODE_PORT=1234 NODE_CONFIG_DIR=./src/server/config/ node src/server/index.js
let port = process.env.NODE_PORT || CONFIG.port || 7203;
CONFIG.port = port;

// Start it up
let server = require('./lib/server');

server.listen(port, function () {
  logger.info(`NPR Fee Server running on port: ${port}`);
});

process.on('SIGINT', function () {
  console.log('\nGracefully shutting down from SIGINT (Ctrl-C)');
  server.close();
  process.exit();
});
