let debug = require('debug')('nf-client-sever');
var logger = console;
let CONFIG = require('config');
import * as exp from 'express';

export function unhandledResponse(
  err: any,
  req: exp.Request,
  res: exp.Response,
  next: exp.NextFunction
) {
  logger.info(JSON.stringify(err));
  logger.info('unhandledResponse');
  logger.error(err);

  // report 500s
  //ErrorReporter.ReportAsync(err);

  var msg = '';

  if (err && err.message) {
    msg = err.message;
  }

  var json = {
    code: 500,
    name: 'unhandled API error',
    message: msg,
    error: {
      message: err.message,
      stack: err.stack,
    },
  };

  return res.status(json.code).json(json);
}

// a non-existant route does not have an err object in signature
export function routeNotFound(
  req: exp.Request,
  res: exp.Response,
  next: exp.NextFunction
) {
  logger.warn(
    'route not found url: %s route: %s',
    req.url || '',
    req.route || ''
  );

  const msg = 'Route does not exist.' + (req.url || '');

  var json = {
    code: 404,
    name: msg,
    message: 'Route does not exist. Check the url.',
  };

  // report 404s
  //ErrorReporter.ReportAsync(new Error(msg));

  return res.status(json.code).json(json);
}

export function itemNotFound(
  req: exp.Request,
  res: exp.Response,
  next: exp.NextFunction
) {
  var json = {
    code: 404,
    name: 'NotFound',
    message: 'Item not found.',
  };
  return res.status(json.code).json(json);
}
