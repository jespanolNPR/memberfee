const CONFIG = require('config');
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as http from 'http';
import { routeNotFound, unhandledResponse } from './errors';
import { StaticAngularRouter } from './static-angular-router';

const app = express();

const httpServer = new http.Server(app);

// express compression middleware for gzip
const gzipCompression = require('compression');

// middleware
app.disable('x-powered-by');
app.use(gzipCompression());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(cookieParser());

app.use('/api/nf-api-v2', require('./nf-api'));
app.use('/api/salesforce-api-v1',require('./salesforce-api'));
//
// Routes to host the static Angular js/hml files
StaticAngularRouter.addRoutes(app);

// this handler gets called as long as we use promises .catch(next)
// if not using promises you must call it yourself with try/catch
app.use(unhandledResponse);

// 404s must be last middleware added
app.use(routeNotFound);

// same as: module.exports = httpServer;
export = httpServer;
