let CONFIG = require('config');
import * as express from 'express';
import * as path from 'path';
import * as _ from 'lodash';

const logger = console;

/**
 * It is important what index.html has for <base href="/" />
 *
 * @export
 * @class StaticAngularRouter
 */
export class StaticAngularRouter {
  
  static setNoCacheHeaders(res: any) {
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    // pragma is for backwards compatability
    res.setHeader('Pragma', 'no-cache');
    /// 0 means its already expired
    res.setHeader('Expires', '0');
  }

  static addRoutes(app: express.Express) {
    const projRoot = path.join(__dirname, '../../../');

    const dirApp = path.join(projRoot, 'dist/client/');
    const indexPath = path.join(projRoot, 'dist/client/index.html');

    logger.info('dir app', dirApp);
    logger.info('index path', indexPath);

    const noCache = (req, res, next) => {
      const url: string = req.url;

      // do not cache any html files
      const dontCache =
        url &&
        (url === '/' ||
          url.endsWith('.html') ||
          url.endsWith('.htm') ||
          url.endsWith('.json'));

      if (dontCache) {
        logger.info(`Cache: Setting no cache headers for url = ${url}`);
        StaticAngularRouter.setNoCacheHeaders(res);
      } else {
        logger.info(`Cache: Allowing caching of url =${url}`);
      }

      next();
    };

    app.use(noCache, express.static(dirApp));
  }
}
